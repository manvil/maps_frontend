This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
This is a very basic app using MapBox.
The default config setup is under src/config.json

* Setup
    * `yarn install`

* Starting
    * `yarn start`

* Improvements
    * To load only property data that are within the scope of the map
        * Currently all properties are loaded assuming that there are limited number.
        * However, if there is a huge number of records then loading would become very slow
        * The idea is to load properties only that is within a selected area or load only
        when the zoom is at particular level or below

    * Use Eslint to ensure that the best practices of ES6 are followed
        * This was omitted only due to lack of time
