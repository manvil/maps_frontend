import React, { Component } from "react";
import MapBox from "./Components/MapBox";
import config from "./config.json";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MapBox accessToken={config.token} server={config.server} style={config.styles.light}
                sprite={config.sprites.light}/>
      </div>
    );
  }
}

export default App;
