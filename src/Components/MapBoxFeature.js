import React, { Component } from "react";
import { Feature, Layer } from "react-mapbox-gl";
import PropTypes from "prop-types";
import $ from "jquery";
import MapBoxCard from "./MapBoxCard";
import 'bootstrap/dist/css/bootstrap.min.css';

class MapBoxFeature extends Component {
  constructor(props) {
    super(props);
    this.state = { "coordinates" : [], "markerData": {} };
  }

  componentWillMount() {
    const url = `${this.props.server}/locations`;
    $.get(url, (data) => {
      this.setState({ "coordinates" : data });
    });
  }

  markerClicked(id) {
    const url = `${this.props.server}/locations/${id}`;
    return () => $.get(url, (data) => this.setState({"markerData": data}));
  }

  render () {
    const features = this.state.coordinates.map(coord =>  {
      return <Feature key={coord[0]} coordinates={coord.slice(1)} onClick={this.markerClicked(coord[0]).bind(this)} />;
    });
    return <div><Layer
      type="symbol"
      id="marker"
      layout={{ "icon-image": "marker-15" }}>
      {features}
    </Layer><MapBoxCard {...this.state.markerData}/></div>;
  }
}

MapBoxFeature.propTypes = {
  "server": PropTypes.string.isRequired,
};

export default MapBoxFeature;
