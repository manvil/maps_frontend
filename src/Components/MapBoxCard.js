import { Card, CardTitle, CardText } from 'reactstrap';
import React, { Component } from "react";
import PropTypes from "prop-types";

class MapBoxCard extends Component {
  render () {
    let card = "";
    if(this.props.id) {
      let { lga_code, council_property_number, full_address, latitude, longitude } = this.props;
      card=<Card body>
        <CardTitle>{council_property_number}</CardTitle>
        <CardText>LGA Code: {lga_code}</CardText>
        <CardText>Address: {full_address}</CardText>
        <CardText>{longitude}, {latitude}</CardText>
      </Card>;
    }
    return <div className="float-sm-right">
      {card}
    </div>;
  }
}

MapBoxCard.propTypes = {
  "id": PropTypes.number,
  "lga_code": PropTypes.number,
  "council_property_number": PropTypes.string,
  "full_address": PropTypes.string,
  "latitude": PropTypes.string,
  "longitude": PropTypes.string,
  "postcode": PropTypes.string
};

export default MapBoxCard;
