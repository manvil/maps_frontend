import React, { Component } from "react";
import ReactMapboxGl from "react-mapbox-gl";
import PropTypes from "prop-types";
import MapBoxFeature from "./MapBoxFeature";
import "mapbox-gl/dist/mapbox-gl.css";

class MapBox extends Component {
  constructor(props) {
    super(props);
    this.state = { "markerData" : null };
  }

  selectedMarker(markerData) {
    console.log(markerData)
    return this.setState({ markerData });
  }

  render () {
    let { accessToken, server, sprite, style } = this.props;
    const Map = ReactMapboxGl({ accessToken });
    return <Map
      sprite={ sprite }
      style={ style }
      center={[144.9631, -37.8136]} // Coordinates of melbourne
      zoom={[5]}
      containerStyle={{
        height: "100vh",
        width: "100vw"
      }}>
      <MapBoxFeature server={ server } />
    </Map>
  }
}

MapBox.propTypes = {
  "accessToken": PropTypes.string.isRequired,
  "server": PropTypes.string.isRequired,
  "sprite": PropTypes.string.isRequired,
  "style": PropTypes.string.isRequired
};

export default MapBox;
